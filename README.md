# README #

[BrainSuite] (c) 2016 Statistics Toolbox (bss)
=========
---------
The [BrainSuite] (c) statistics toolbox allows the application of advanced statistical models to surface, image and curve based outputs generated from BrainSuite. 
This enables population or group modeling of cortical or sulcal morphology. 

